from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

import os
import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')

def test_files(host):
    files = [
        "/opt/nifi/bin/nifi.sh"
    ]
    for file in files:
        f = host.file(file)
        assert f.exists
        assert f.is_file

def test_user(host):
    assert host.group("nifi").exists
    assert "nifi" in host.user("nifi").groups
    assert host.user("nifi").shell == "/usr/sbin/nologin"

def test_service(host):
    s = host.service("nifi")
    assert s.is_enabled
    assert s.is_running
